# Decisiones de diseño

* El banco conoce a su ATM y home banking y el ATM y home banking conocen a su banco. Esta relación se concreta cuando se setea el banco en la ATM y/o home banking.
* El banco conoce a sus clientes y los clientes saben de qué banco son. Esta relación se concreta cuando se setea el banco en el cliente.
* Login devuelve true para un ingreso exitoso y false para un ingreso fallido.
* La cuenta de home banking se inicializa con un cliente, email y password.
