Feature: Login
  As a customer
  I want to login
  In order to perform operatios

  Scenario: Successful login
    Given my user is "nicopaez@gmail.com" and password "Pasw0rd!"
    When I login with "nicopaez@gmail.com" and password "Pasw0rd!"
    Then I access the system

  Scenario: Successful failed
    Given my user is "nicopaez@gmail.com" and password "Pasw0rd!"
    When I login with "nicopaez@gmail.com" and password "sssssasdasdadas"
    Then I do not access the system
    