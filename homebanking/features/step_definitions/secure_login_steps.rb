Given(/^I have already registered$/) do
  @bank = Bank.new
  @atm = Atm.new(@bank)
  @customer = Customer.new(@bank)
  @atm.generate_code(@customer, "1")
  @home_banking = HomeBanking.new(@bank)
  @customer.register("nicopaez@gmail.com", "Pasw0rd!", "1")
end

When(/^I attemp (\d+) failed logins$/) do |failed_logins|
  for i in 1..failed_logins.to_i
    @result = @customer.login("nicopaez@gmail.com", "p")
  end
end

Then(/^my credentials get blocked$/) do
  expect(@home_banking.account("nicopaez@gmail.com")).to be_blocked
end

