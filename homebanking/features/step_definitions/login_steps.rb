Given(/^my user is "([^"]*)" and password "([^"]*)"$/) do |user, password|
  @bank = Bank.new
  @atm = Atm.new(@bank)
  @customer = Customer.new(@bank)
  @atm.generate_code(@customer, "1")
  @home_banking = HomeBanking.new(@bank)
  @customer.register(user, password, "1")
end

When(/^I login with "([^"]*)" and password "([^"]*)"$/) do |user, password|
  @result = @customer.login(user, password)
end

Then(/^I access the system$/) do
  expect(@result).to eq true
end

Then(/^I do not access the system$/) do
  expect(@result).to eq false
end

