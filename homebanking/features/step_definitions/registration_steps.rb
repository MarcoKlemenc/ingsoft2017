Given(/^the ATM generated code "([^"]*)"$/) do |code|
  @bank = Bank.new
  @atm = Atm.new(@bank)
  @customer = Customer.new(@bank)
  @home_banking = HomeBanking.new(@bank)
  @atm.generate_code(@customer, code)
end

When(/^I register with email "([^"]*)", password "([^"]*)" and code "([^"]*)"$/) do |email, password, code|
  @email = email
  @customer.register(@email, password, code)
end

Then(/^my account is created$/) do
  expect(@home_banking.account(@email)).not_to be nil
end

Then(/^my account is not created$/) do
  expect(@home_banking.account(@email)).to be nil
end

