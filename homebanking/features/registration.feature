Feature: Registration
  As a customer
  I want to register my account
  In order to perform operations

  Scenario: Successful registration
    Given the ATM generated code "123456"
    When I register with email "jd@free.com", password "Passw0rd!" and code "123456"
    Then my account is created

  Scenario: Failed registration - ATM code invalid
    Given the ATM generated code "123456"
    When I register with email "jd@free.com", password "Passw0rd!" and code "111111"
    Then my account is not created

  Scenario: Failed registration - password without uppercase
    Given the ATM generated code "123456"
    When I register with email "jd@free.com", password "password!" and code "123456"
    Then my account is not created

  Scenario: Failed registration - password too short
    Given the ATM generated code "123456"
    When I register with email "jd@free.com", password "Pa" and code "123456"
    Then my account is not created