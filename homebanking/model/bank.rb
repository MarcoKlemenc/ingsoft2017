class Bank

  attr_accessor :atm, :home_banking
  attr_reader :customers
  
  def initialize()
    @customers = Set.new
  end
  
  def add_customer(customer)
    @customers.add(customer)
  end
  
  def has_customer?(customer)
    return @customers.include?(customer)
  end
  
end
