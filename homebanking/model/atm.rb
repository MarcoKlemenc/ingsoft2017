class Atm

  attr_reader :codes, :bank
  
  def initialize(new_bank)
    @codes = Hash.new
    self.bank = new_bank
  end
  
  def bank=(new_bank)
    @bank = new_bank
    new_bank.atm = self
  end
  
  def code(customer)
    return @codes[customer]
  end
  
  def generate_code(customer, new_code)
    @codes[customer] = new_code
  end
  
end
