class HomeBankingAccount

  attr_reader :customer, :email, :password, :consecutive_failures
  
  MAX_CONSECUTIVE_FAILURES = 3

  def initialize(customer, email, password)
    @customer = customer
    @email = email
    @password = password
    @consecutive_failures = 0
  end
  
  def login(password)
    if @password == password && !self.blocked?()
      @consecutive_failures = 0
      return true
    end
    @consecutive_failures += 1
    return false
  end
  
  def blocked?()
    return @consecutive_failures >= MAX_CONSECUTIVE_FAILURES
  end

end
