class Customer

  attr_reader :bank
  attr_accessor :account
  
  def initialize(new_bank)
    self.bank = new_bank
  end
  
  def bank=(new_bank)
    @bank = new_bank
    new_bank.add_customer(self)
  end
  
  def login(user, password)
    return @bank.home_banking.login(user, password)
  end
  
  def register(user, password, code)
    return @bank.home_banking.register(self, user, password, code)
  end

end
