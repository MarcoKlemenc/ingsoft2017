class HomeBanking

  attr_reader :bank
  
  MIN_PASSWORD_LENGTH = 8

  def initialize(new_bank)
    @accounts = Hash.new
    self.bank = new_bank
  end
  
  def bank=(new_bank)
    @bank = new_bank
    new_bank.home_banking = self
  end
  
  def account(email)
    account_clone = @accounts[email].clone if @accounts.key?(email)
  end
  
  def validate_password(password)
    return password.length >= MIN_PASSWORD_LENGTH && password =~ /[A-Z]/ && password =~ /[0-9]/ && password =~ /[^a-zA-Z0-9]/
  end
  
  def login(user, password)
    return @accounts.key?(user) && @accounts[user].login(password)
  end
  
  def register(customer, user, password, code)
    if validate_password(password) && !@accounts.key?(user) && @bank.atm.code(customer) == code
      account = HomeBankingAccount.new(customer, user, password)
      @accounts[user] = account
      customer.account = account
    end
  end

end
