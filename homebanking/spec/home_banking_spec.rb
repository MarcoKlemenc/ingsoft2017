require 'rspec' 
require_relative '../model/home_banking'

describe 'HomeBanking' do

  subject(:home_banking) { HomeBanking.new(@bank) }  
  
  before(:each) do
    @bank = double('Bank')
    @atm = double('Atm')
    allow(@bank).to receive(:home_banking=)
  end
   
  it 'home banking can login' do
    customer = double('Customer')
    allow(@atm).to receive(:code).with(customer).and_return("123456")
    allow(@bank).to receive(:atm).and_return(@atm)
    allow(customer).to receive(:account=)
    home_banking.register(customer, "nicopaez@gmail.com","Pasw0rd!", "123456")
    expect(home_banking.login("nicopaez@gmail.com","Pasw0rd!")).to eq true
  end
  
  it 'login should return false with non-existent user' do
    expect(home_banking.login("nico_paez@gmail.com","Pasw0rd!")).to eq false
  end

  it 'should register successfully with valid code' do
    customer = double('Customer')
    allow(@atm).to receive(:code).with(customer).and_return("123456")
    allow(@bank).to receive(:atm).and_return(@atm)
    allow(customer).to receive(:account=)
    home_banking.register(customer, "nicopaez@gmail.com","Pasw0rd!", "123456")
    expect(home_banking.account("nicopaez@gmail.com").password).to eq("Pasw0rd!")
  end

  it 'should link customer to account upon successful registration' do
    customer = double('Customer')
    allow(@atm).to receive(:code).with(customer).and_return("123456")
    allow(@bank).to receive(:atm).and_return(@atm)
    expect(customer).to receive(:account=).once
    home_banking.register(customer, "nicopaez@gmail.com","Pasw0rd!", "123456")
  end
  
  it 'should not register successfully with invalid code' do
    customer = double('Customer')
    allow(@atm).to receive(:code).with(customer).and_return("123456")
    allow(@bank).to receive(:atm).and_return(@atm)
    home_banking.register(customer, "nicopaez@gmail.com","Pasw0rd!", "111111")
    expect(home_banking.account("nicopaez@gmail.com")).to eq(nil)
  end

  it 'should not register successfully with password without uppercase' do
    customer = double('Customer')
    allow(@atm).to receive(:code).with(customer).and_return("123456")
    home_banking.register(customer, "nicopaez@gmail.com","pasw0rd!", "123456")
    expect(home_banking.account("nicopaez@gmail.com")).to eq(nil)
  end

  it 'should not register successfully with password without number' do
    customer = double('Customer')
    allow(@atm).to receive(:code).with(customer).and_return("123456")
    home_banking.register(customer, "nicopaez@gmail.com","Pasword!", "123456")
    expect(home_banking.account("nicopaez@gmail.com")).to eq(nil)
  end

  it 'should not register successfully with password without symbol' do
    customer = double('Customer')
    allow(@atm).to receive(:code).with(customer).and_return("123456")
    home_banking.register(customer, "nicopaez@gmail.com","Passw0rd", "123456")
    expect(home_banking.account("nicopaez@gmail.com")).to eq(nil)
  end

  it 'should not register successfully with short password' do
    customer = double('Customer')
    allow(@atm).to receive(:code).with(customer).and_return("123456")
    home_banking.register(customer, "nicopaez@gmail.com","Paw0rd!", "123456")
    expect(home_banking.account("nicopaez@gmail.com")).to eq(nil)
  end

  it 'should not register successfully with existing email' do
    customer = double('Customer')
    allow(@atm).to receive(:code).with(customer).and_return("123456")
    allow(@bank).to receive(:atm).and_return(@atm)
    allow(customer).to receive(:account=)
    home_banking.register(customer, "nicopaez@gmail.com","Pasw0rd!", "123456")
    home_banking.register(customer, "nicopaez@gmail.com","Pasw0rd!!", "123456")
    expect(home_banking.account("nicopaez@gmail.com").password).to eq("Pasw0rd!")
  end

end
