require 'rspec' 
require_relative '../model/home_banking_account'

describe 'HomeBankingAccount' do

  subject(:account) { HomeBankingAccount.new(nil, "nicopaez@gmail.com", "Pasw0rd!") }  

  it 'account should save customer' do
    customer = double('Customer')
    account = HomeBankingAccount.new(customer, "nicopaez@gmail.com", "Pasw0rd!")
    expect(account.customer).to eq customer
  end
   
  it 'login should return true with valid password' do
    expect(account.login("Pasw0rd!")).to eq true
  end
  
  it 'consecutive failures should reset on a successful login' do
    account.login("Pasw0rd!!")
    account.login("Pasw0rd!")
    expect(account.consecutive_failures).to eq 0
  end

  it 'login should return false with invalid password' do
    expect(account.login("Pasw0rd!!")).to eq false
  end

  it 'consecutive failures should increase on a failed login' do
    account.login("Pasw0rd!!")
    expect(account.consecutive_failures).to eq 1
  end
  
  it 'should get blocked after three consecutive attempts' do
    account.login("Pasw0rd!!")
    account.login("Pasw0rd!!")
    account.login("Pasw0rd!!")
    expect(account).to be_blocked
  end

  it 'should not login if it got blocked' do
    account.login("Pasw0rd!!")
    account.login("Pasw0rd!!")
    account.login("Pasw0rd!!")
    expect(account.login("Pasw0rd!")).to eq false
  end
  
end
