require 'rspec' 
require_relative '../model/customer'

describe 'Customer' do

  subject(:customer) { Customer.new(@bank) }  
  
  before(:each) do
    @bank = double('Bank')
    allow(@bank).to receive(:add_customer)
  end

  it 'customer is added to bank' do
    expect(@bank).to receive(:add_customer).once  
    customer = Customer.new(@bank)
  end
   
  it 'customer can login' do
    home_banking = double('HomeBanking')
    allow(@bank).to receive(:home_banking).and_return(home_banking)
    allow(home_banking).to receive(:login).with("nicopaez@gmail.com","Pasw0rd!").and_return(true)
    expect(customer.login("nicopaez@gmail.com","Pasw0rd!")).to eq true
  end
  
  it 'customer can register' do
    allow(@bank).to receive(:add_customer)
    home_banking = double('HomeBanking')
    allow(@bank).to receive(:home_banking).and_return(home_banking)
    allow(home_banking).to receive(:register).with(customer, "nicopaez@gmail.com","Pasw0rd!", "123456").and_return(true)
    expect(customer.register("nicopaez@gmail.com","Pasw0rd!", "123456")).to eq true
  end

end
