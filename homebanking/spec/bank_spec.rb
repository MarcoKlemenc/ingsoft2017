require 'rspec' 
require_relative '../model/bank'

describe 'Bank' do

  subject(:bank) { Bank.new }  
   
  it 'bank can add customers' do
    customer = double('Customer')
    bank.add_customer(customer)
    expect(bank.has_customer?(customer)).to eq true
  end
  
end
