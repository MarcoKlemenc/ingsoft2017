require 'rspec' 
require_relative '../model/atm'

describe 'Atm' do

  subject(:atm) { Atm.new(@bank) }  
  
  before(:each) do
    @bank = double('Bank')
    allow(@bank).to receive(:atm=)
  end
   
  it 'atm can set and get codes' do
    customer = double('Customer')
    code = "123456"
    atm.generate_code(customer, code)
    expect(atm.code(customer)).to eq code
  end

  it 'atm is linked to bank' do
    expect(@bank).to receive(:atm=).once
    atm = Atm.new(@bank)
  end
  
end
