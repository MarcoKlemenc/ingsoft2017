Given(/^operand A is (\d+)$/) do |opA|
  @operandA = opA.to_i
end

Given(/^operand B is (\d+)$/) do |opB|
  @operandB = opB.to_i  
end

When(/^I add them$/) do
  calculator = Calculator.new
  @result = calculator.add(@operandA, @operandB)
end

Then(/^the result is (\d+)$/) do |r|
  expect(@result).to eq r.to_i
end
