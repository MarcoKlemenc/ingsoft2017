# Decisiones de diseño

* Existen dos clases (DiscountMapper y TaxMapper) que obtienen el porcentaje correspondiente para un valor o código de estado.
* El método tax de tax_mapper devuelve 0 para aquellos códigos que no estén contemplados.
* El método discount de discount_mapper devuelve 0 para valores no numéricos.
* Los porcentajes obtenidos con las dos clases anteriores se aplican dentro del método calculate.
* Si se ingresa un estado cuyo código no tiene un impuesto asociado, no se aplica impuesto.
* Si se ingresa un valor no numérico o menor o igual a 0 en cantidad o precio unitario, se pedirá un valor correcto.
* A la calculadora se le inyecta un mapeador de descuentos mediante el constructor.
