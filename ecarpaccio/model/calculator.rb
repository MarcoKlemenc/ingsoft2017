class Calculator

  attr_reader :discount_mapper

  def initialize(discount_mapper)
    @discount_mapper = discount_mapper
  end

  def calculate(quantity, unit_price, tax)
    subtotal = quantity * unit_price
    discount = discount_mapper.discount(subtotal)
    total = subtotal * (1 - discount / 100.0) * (1 + tax / 100.0)
    return total.round(2)
  end

end
