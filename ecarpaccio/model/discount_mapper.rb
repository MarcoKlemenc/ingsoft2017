require 'ostruct'

class DiscountMapper

  attr_reader :discounts

  def initialize()
    inf = 1.0 / 0.0
    @discounts = [OpenStruct.new(range: (1000.0...5000.0), discount: 3),
                  OpenStruct.new(range: (5000.0...7000.0), discount: 5),
                  OpenStruct.new(range: (7000.0...10000.0), discount: 7),
                  OpenStruct.new(range: (10000.0...50000.0), discount: 10),
                  OpenStruct.new(range: (50000.0...inf), discount: 15)]
  end
  
  def discount(amount)
    discount_range = @discounts.find {|discount_range| discount_range.range.cover?(amount) }
    unless discount_range.nil?
      discount_range.discount
    else
      0
    end
  end

end
