class TaxMapper

  attr_reader :taxes

  def initialize()
    @taxes = {
      "UT" => 6.85,
      "NV" => 8,
      "TX" => 6.25,
      "AL" => 4,
      "CA" => 8.25
    }
    @taxes.default = 0
  end
  
  def tax(state_code)
    return taxes[state_code]
  end

end
