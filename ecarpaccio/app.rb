require_relative 'model/discount_mapper'
require_relative 'model/tax_mapper'
require_relative 'model/calculator'

quantity = 0
puts "Ingrese la cantidad (sin unidad y con . para decimales):"
while quantity <= 0  
  quantity = gets.strip.to_f
  if quantity <= 0
    puts "Cantidad no válida. Intente nuevamente."
  end
end

unit_price = 0
puts "Ingrese el precio unitario (sin signo $ y con . para decimales):"
while unit_price <= 0
  unit_price = gets.strip.to_f
  if unit_price <= 0
    puts "Precio unitario no válido. Intente nuevamente."
  end
end

puts "Ingrese el código de estado:"
state_code = gets.strip

calculator = Calculator.new(DiscountMapper.new)
tax_mapper = TaxMapper.new
tax = tax_mapper.tax(state_code)

puts "El monto es:"
puts calculator.calculate(quantity, unit_price, tax)
