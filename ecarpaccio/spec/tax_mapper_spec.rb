require 'rspec' 
require_relative '../model/tax_mapper'

describe 'TaxMapper' do

  let(:tax_mapper) { TaxMapper.new }  
   
  it 'tax on UT should return 6.85' do
    expect(tax_mapper.tax('UT')).to eq 6.85
  end

  it 'tax on NV should return 8' do
    expect(tax_mapper.tax('NV')).to eq 8
  end

  it 'tax on TX should return 6.25' do
    expect(tax_mapper.tax('TX')).to eq 6.25
  end

  it 'tax on AL should return 4' do
    expect(tax_mapper.tax('AL')).to eq 4
  end

  it 'tax on CA should return 8.25' do
    expect(tax_mapper.tax('CA')).to eq 8.25
  end

  it 'tax on AZ should return 0' do
    expect(tax_mapper.tax('AZ')).to eq 0
  end

end
