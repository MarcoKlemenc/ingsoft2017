require 'rspec' 
require_relative '../model/calculator'

describe 'Calculator' do

  let(:mapper) { double('DiscountMapper') }  
  let(:calculator) { Calculator.new(mapper) }  

  it 'should be initialized with discount mapper' do
    expect(calculator.discount_mapper).to eq mapper
  end
   
  it 'calculate 20 units at $20 each with 4% tax should return $416' do
    value = 20 * 20
    allow(mapper).to receive(:discount).with(value).and_return(0)
    expect(calculator.calculate(20,20,4)).to eq 416
  end

end
