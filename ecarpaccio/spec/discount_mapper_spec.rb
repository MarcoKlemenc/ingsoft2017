require 'rspec' 
require_relative '../model/discount_mapper'

describe 'DiscountMapper' do

  let(:discount_mapper) { DiscountMapper.new }  
   
  it 'discount on $400.50 should return 0' do
    expect(discount_mapper.discount(400.5)).to eq 0
  end

  it 'discount on $1000 should return 3' do
    expect(discount_mapper.discount(1000)).to eq 3
  end

  it 'discount on $2500.65 should return 3' do
    expect(discount_mapper.discount(2500.65)).to eq 3
  end

  it 'discount on $5000 should return 5' do
    expect(discount_mapper.discount(5000)).to eq 5
  end

  it 'discount on $6000.40 should return 5' do
    expect(discount_mapper.discount(6000.4)).to eq 5
  end

  it 'discount on $7000 should return 7' do
    expect(discount_mapper.discount(7000)).to eq 7
  end

  it 'discount on $9000.25 should return 7' do
    expect(discount_mapper.discount(9000.25)).to eq 7
  end

  it 'discount on $10000 should return 10' do
    expect(discount_mapper.discount(10000)).to eq 10
  end

  it 'discount on $35000.85 should return 10' do
    expect(discount_mapper.discount(35000.85)).to eq 10
  end

  it 'discount on $50000 should return 15' do
    expect(discount_mapper.discount(50000)).to eq 15
  end

  it 'discount on $3847500.15 should return 15' do
    expect(discount_mapper.discount(3847500.15)).to eq 15
  end

  it 'discount on $0 should return 0' do
    expect(discount_mapper.discount(0)).to eq 0
  end

  it 'discount on $-5 should return 0' do
    expect(discount_mapper.discount(-5)).to eq 0
  end

  it 'discount on $AAA should return 0' do
    expect(discount_mapper.discount("AAA")).to eq 0
  end

end
