Given(/^(\d+) items were sold$/) do |arg1|
  @quantity = arg1.to_f
end

Given(/^the unit price is (\d+)$/) do |arg1|
  @unit_price = arg1.to_f
end

Given(/^the state code is "([^"]*)"$/) do |arg1|
  tax_mapper = TaxMapper.new
  @tax = tax_mapper.tax(arg1)
end

When(/^I calculate the total price$/) do
  calculator = Calculator.new(DiscountMapper.new)
  @amount = calculator.calculate(@quantity, @unit_price, @tax)
end

Then(/^the result is (.+)$/) do |arg1|
  expect(@amount).to eq arg1.to_f
end

