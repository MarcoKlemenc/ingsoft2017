Feature: Output the total price
  As a salesman
  I want to calculate the total price of an order
  So I don't have to calculate it manually and can sell faster

  Scenario Outline: Sale
    Given <qty> items were sold
    And the unit price is <price>
    And the state code is <code>
    When I calculate the total price
    Then the result is <total>
  
  Examples:
    | qty | price | code |  total  |
    | 20  |  20   | "NV" |   432   |
    | 30  |  25   | "TX" |   796.88|
    | 50  |  60   | "UT" |  3109.34|
    | 100 |  40   | "CA" |  4200.10|
    | 30  |  200  | "AL" |  5928   |
    | 110 |  50   | "NV" |  5643   |
    | 80  |  100  | "TX" |  7905   |
    | 140 |  70   | "UT" |  9738.31|
    | 100 |  400  | "CA" | 38970   |
    | 1200|  40   | "AL" | 44928   |
    | 300 |  200  | "NV" | 55080   |
    | 400 |  300  | "TX" |108375   |
    | 50  |  20   | "UT" |  1036.45|
    | 100 |  50   | "CA" |  5141.88|
    | 35  |  200  | "AL" |  6770.40|
    | 100 |  100  | "NV" |  9720   |
    | 250 |  200  | "TX" | 45156.25|

